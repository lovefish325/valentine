$(function () {
    setTimeout(function () {
        $('#setBubbles').css("transition", "2s ease");
        $('#setBubbles').css("-webkit-transition", "2s ease");
        $('#setBubbles').css("opacity", "0.1");
        $('#setWindow').css("transition", "1s ease");
        $('#setWindow').css("-webkit-transition", "1s ease");
        $('#setWindow').css("background-color", "unset");
    }, 4000);
})
